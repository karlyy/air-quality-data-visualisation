library(ncdf4)
library(chron)
library(lattice)
library(RColorBrewer)
library(ggplot2) # package for plotting
library(sf)
library(rnaturalearth)

#setwd("C:/Users/Karolina/Desktop/dataViz/InfoVisProject/air-quality-data-visualisation")


nc_file <- nc_open("daymean20120828.nc")

print(nc_file)

# get lon
lon <- ncvar_get(nc_file,"lon")
nlon <- dim(lon)
head(lon)

#get lat
lat <- ncvar_get(nc_file,"lat")
nlat <- dim(lat)
head(lat)

print(c(nlon,nlat))


# get level
level <- ncvar_get(nc_file,"level")
level

#get pm
pm_array <- ncvar_get(nc_file,"pm10_conc")
pmname <- ncatt_get(nc_file,"pm10_conc","long_name")
pmunits <- ncatt_get(nc_file,"pm10_conc","units")
fillvalue <- ncatt_get(nc_file,"pm10_conc","_FillValue")
dim(pm_array)

# get global attributes
title <- ncatt_get(nc_file,0,"title")
institution <- ncatt_get(nc_file,0,"institution")
datasource <- ncatt_get(nc_file,0,"source")
references <- ncatt_get(nc_file,0,"references")
history <- ncatt_get(nc_file,0,"history")
Conventions <- ncatt_get(nc_file,0,"Conventions")

nc_close(nc_file) 

# replace netCDF fill values with NA's
pm_array[pm_array==fillvalue$value] <- NA

dim(pm_array)

# get a single slice or layer (level 1)
m <- 1
pm_slice <- pm_array[,,m]
dim(pm_slice)

# quick map
#image(lon,lat,pm_slice, col=rev(brewer.pal(10,"RdBu")))

df <- expand.grid(lon = lon, lat = lat)
df$pm_slice <- as.vector(pm_slice)


# Create the ggplot plot
ggplot(df, aes(x = lon, y = lat, fill = pm_slice)) +
  geom_raster() +
  scale_fill_distiller(palette = "RdBu", direction = -1, na.value = "white") +
  coord_fixed() +
  labs(fill = pmname$value, title = title$value) +
  theme_minimal()



# Define the range of longitude and latitude
min_lon <- floor(min(lon))
max_lon <- ceiling(max(lon))
min_lat <- floor(min(lat))
max_lat <- ceiling(max(lat))

world_map <- map_data("world")

# Filter the map data within the specified range
filtered_map <- subset(world_map, 
                       long >= min_lon & long <= max_lon & lat >= min_lat & lat <= max_lat)

# Plot the map
map_plot <- ggplot() +
  geom_polygon(data = filtered_map, aes(x = long, y = lat, group = group), fill = "lightblue") +
  coord_fixed() +
  xlim(min_lon, max_lon) +
  ylim(min_lat, max_lat) +
  theme_minimal()

#map_with_pm <- map_plot +
 # geom_raster(data = df, aes(x = lon, y = lat, fill = pm_slice)) +
  #scale_fill_distiller(palette = "RdBu", direction = -1, na.value = "white") +
  #labs(fill = pmname$value, title = title$value)

#print(map_with_pm)

ggplot_map_with_pm_transparent <- map_plot +
  geom_raster(data = df, aes(x = lon, y = lat, fill = pm_slice, alpha = 0.8)) +
  scale_fill_distiller(palette = "Blues", direction = 1, na.value = "white") +
  labs(fill = pmname$value, title = title$value)

# Display the final plot with the map and PM data (transparent raster)
print(ggplot_map_with_pm_transparent)


#https://en.wikipedia.org/wiki/Contour_line#Barometric_pressure
# Create the ggplot contour plot
ggplot_map_with_pm_contour <- map_plot +
  geom_contour(data = df, aes(x = lon, y = lat, z = pm_slice), 
               bins = 15, color = "black", alpha = 0.6) +
  scale_fill_distiller(palette = "Blues", direction = -1, na.value = "white") +
  labs(fill = pmname$value, title = title$value)

# Display the final plot with the map and PM data (contour plot)
print(ggplot_map_with_pm_contour)


# Create the ggplot heatmap
ggplot_map_with_pm_heatmap <- map_plot +
  geom_tile(data = df, aes(x = lon, y = lat, fill = pm_slice), alpha = 0.5) +
  scale_fill_distiller(palette = "Blues", direction = 1, na.value = "white") +
  labs(fill = pmname$value, title = title$value)

# Display the final plot with the map and PM data (heatmap)
print(ggplot_map_with_pm_heatmap)



